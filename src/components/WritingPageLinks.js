
import React from 'react' ;
import { Link } from 'react-router-dom' ;


class WritingPageLinks extends React.Component {

    render () {
        const linksInfo = [
            {
                "icon" : "las la-lightbulb" ,
                "link" : "" ,
                "tooltip" : "philosophy"
            } ,
            {
                "icon" : "las la-tree" ,
                "link" : "https://www.thetechnodruid.com" ,
                "tooltip" : "spirituality"
            } ,
            {
                "icon" : "las la-pen-fancy" ,
                "link" : "" ,
                "tooltip" : "creative"
            } ,
            {
                "icon" : "las la-comment-dots" ,
                "link" : "" ,
                "tooltip" : "blog"
            }
        ] ;

        return (
            <div className="writing-page-links">
            { linksInfo.map(item => (
                <div key={item.tooltip}>
                    <div className='writing-page-link-director'>
                        <a href={item.link}>
                            <div className='writing-page-link'>
                                <i className={item.icon}></i>
                            </div>
                        </a>
                    </div>
                    <div className='writing-page-link-tooltip'>
                        {item.tooltip.toUpperCase()}
                    </div>
                </div>
            ))}
            </div>
        ) ;
    }
}

export default WritingPageLinks ;