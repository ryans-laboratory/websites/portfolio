
import React from 'react' ;

import HomePageHeader from './HomePageHeader' ;
import AboutPageHeader from './AboutPageHeader' ;
import WritingPageHeader from './WritingPageHeader' ;
import SoftwarePageHeader from './SoftwarePageHeader';
import ArchitecturePageHeader from './ArchitecturePageHeader';

class Header extends React.Component {

    render() {
        let subheader ;

        if (this.props.page === "/about") {
            subheader = <AboutPageHeader />
        } else if (this.props.page === "/software") {
            subheader = <SoftwarePageHeader />
        } else if (this.props.page === "/writing") {
            subheader = <WritingPageHeader />
        } else if (this.props.page === "/architecture") {
            subheader = <ArchitecturePageHeader />  
        } else {
            subheader = <div></div>
        }

        return (
            <div>
                <HomePageHeader />
                {subheader}
            </div>
        )
    }
}

export default Header ;