
import React from 'react' ;
import { Link } from 'react-router-dom' ;


class HomePageLinks extends React.Component {

    render() {
        const linksInfo = [
            {
                "icon" : 'lar la-user' ,
                "link" : "/about" ,
                "tooltip" : "ABOUT ME" ,
                "color" : "yellow" ,
            } ,
            {
                "icon" : 'las la-code' ,
                "link" : "/software" ,
                "tooltip" : "SOFTWARE" ,
                "color" : "red" ,
            } ,
            {
                "icon" : 'las la-feather-alt' ,
                "link" : "/writing" ,
                "tooltip" : "WRITING" ,
                "color" : "blue" ,
            } ,
            {
                "icon" : 'las la-archway' ,
                "link" : "/architecture" ,
                "tooltip" : "ARCHITECTURE" ,
                "color" : "brown" ,
            }
        ] ;

        return (
            <div id='home-page-main-links'>
            { linksInfo.map(item => (
                <div key={item.tooltip}>
                    <div className='home-page-link-director'>
                        <Link to={item.link}>
                            <div className='home-page-link'>
                                <i className={item.icon}></i>
                            </div>
                        </Link>
                    </div>
                    <div className='home-page-link-tooltip'>
                        {item.tooltip.toUpperCase()}
                    </div>
                    <div className={'home-page-link-banner ' + item.color }></div>
                </div>
            ))}
            </div>
        ) ;
    }
}

export default HomePageLinks ;