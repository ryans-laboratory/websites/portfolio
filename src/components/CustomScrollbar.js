
import { Scrollbars } from 'react-custom-scrollbars-2';
import React from 'react' ;


class CustomScrollbar extends React.Component {

    render() {
        return(
            <Scrollbars
                style={{ height : "100vh" , width : "100vw" }}
                autoHide 
                autoHideDuration={300} 
                autoHideTimeout={1000}
                // renderTrackHorizontal={ props => <div { ...props } className="srollbar-horizontal" />}
                // renderTrackVertical={ props => <div { ...props } className="scrollbar-vertical" />}
                renderThumbHorizontal={ props => <div { ...props } className="scrollbar-thumb-horizontal" />}
                renderThumbVertical={ props => <div { ...props } className="scrollbar-thumb-vertical" />}
            >
                {this.props.children}
            </Scrollbars>
        ) ;    
    }
}

export default CustomScrollbar ;