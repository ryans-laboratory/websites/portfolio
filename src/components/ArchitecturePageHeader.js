
import React from 'react' ;


class ArchitecturePageHeader extends React.Component {

    render() {
        return (
            <div className='architecture-page-header'>
                <div className='page-header-text'>ARCHITECTUR<span>E</span></div>
                <div className='page-quote architecture-page-quote'>The human world is unique in that it's built with speed and intent.</div>
            </div>
        ) ;
    }
}

export default ArchitecturePageHeader ;