
import React from 'react' ;


class SoftwarePageHeader extends React.Component {

    render() {
        return (
            <div className='software-page-header'>
                <div className='page-header-text'>SOFTWAR<span>E</span></div>
                <div className='page-quote software-page-quote'>Looking back and thinking "Why would I do that?!" is a sign of growth. It also means there's not much here right now.</div>
            </div>
        ) ;
    }
}

export default SoftwarePageHeader ;