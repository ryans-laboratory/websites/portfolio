
import React from 'react' ;


class WritingPageHeader extends React.Component {

    render() {
        return (
            <div className='writing-page-header'>
                <div className='page-header-text'>WRITIN<span>G</span></div>
                <div className='writing-page-quote page-quote'>I've been cursed with always having something to say</div>
            </div>
        ) ;
    }
}

export default WritingPageHeader ;