
import React from 'react' ;


class AboutPageHeader extends React.Component {

    render() {
        return (
            <div className='about-page-header'>
                <div className='page-header-text'>ABOUT M<span>E</span></div>
                <div className='page-quote about-page-quote'>Raleigh-based nerd on a mission to create a future that's more human.</div>
            </div>
        ) ;
    }
}

export default AboutPageHeader ;