
import React from 'react' ;
import { Link } from 'react-router-dom' ;


class HomePageHeader extends React.Component {

    render() {
        return (
            <Link to="/">
                <div className='home-page-header'>
                    <div className='home-page-header-text'>RYAN DE KLER<span>K</span></div>
                    <div className='page-header-highlight green'></div>
                </div>
            </Link>
        ) ;
    }
}

export default HomePageHeader ;