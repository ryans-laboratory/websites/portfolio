
import React from 'react' ;
import { Link } from 'react-router-dom' ;


class ArchitecturePageLinks extends React.Component {

    render () {
        const linksInfo = [
            {
                "icon" : "las la-folder-open" ,
                "link" : "" ,
                "tooltip" : "portfolio"
            } ,
            {
                "icon" : "las la-feather-alt" ,
                "link" : "" ,
                "tooltip" : "writing"
            }
        ] ;

        return (
            <div className="architecture-page-links">
            { linksInfo.map(item => (
                <div key={item.tooltip}>
                    <div className='architecture-page-link-director'>
                        <a href={item.link}>
                            <div className='architecture-page-link'>
                                <i className={item.icon}></i>
                            </div>
                        </a>
                    </div>
                    <div className='architecture-page-link-tooltip'>
                        {item.tooltip.toUpperCase()}
                    </div>
                </div>
            ))}
            </div>
        ) ;
    }
}

export default ArchitecturePageLinks ;