
import { 
  Routes ,
  Route 
} from 'react-router-dom' ;

import HomePage from './pages/HomePage' ;
import AboutPage from './pages/AboutPage' ;
import SoftwarePage from './pages/SoftwarePage' ;
import WritingPage from './pages/WritingPage' ;
import ArchitecturePage from './pages/ArchitecturePage' ;

export default function Portfolio() {
  return (
    <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/about" element={<AboutPage />} />
        <Route path="/software" element={<SoftwarePage />}></Route>
        <Route path="/writing" element={<WritingPage />}></Route>
        <Route path="/architecture" element={<ArchitecturePage />}></Route>
    </Routes>
  );
}