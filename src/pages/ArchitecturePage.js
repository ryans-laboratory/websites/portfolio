import React from 'react' ;
import { Link, useLocation } from 'react-router-dom' ;


import CustomScrollbar from '../components/CustomScrollbar' ;
import Header from '../components/Header'
import Footer from '../components/Footer'
import ArchitecturePageLinks from '../components/ArchitecturePageLinks';


function ArchitecturePage () {
    let location = useLocation() ;

    return (
        <CustomScrollbar>
            <div id="arch-container" className="container">
                <div className="header">
                    <Header page={location.pathname}/>
                </div>

                <div className="body">
                    <div id="architecture-page-content">
                        <ArchitecturePageLinks />

                        <div className='architecture-page-description'>
                            <p>
                                Architecture is the design of the human world. That design, however, is not made for humans.
                            </p>
                            <p>
                                I have a <Link to='portfolio' className='portfolio'>PORTFOLIO</Link> that has a curated list of my ideas made
                                more visible and physical.
                            </p>
                            <p>
                                I also have some <a href='' className='writing'>WRITING</a> about architecture where I explain my ideas and 
                                attempt a critique of the industry as a whole.
                            </p>
                        </div>
                    </div>
                </div>

                <div className="footer">
                    <Footer page={location.pathname} />
                </div>
            </div>
        </CustomScrollbar>
    ) ;
}

export default ArchitecturePage ;