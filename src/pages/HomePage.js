
import React from 'react' ;
import { useLocation } from 'react-router-dom' ;

import HomePageLinks from '../components/HomePageLinks' ;
import Header from '../components/Header'
import Footer from '../components/Footer'


function HomePage () {
    let location = useLocation() ;

    return (
        <div id="home-container" className="container">
            <div className="header">
                <Header page={location.pathname}/>
            </div>

            <div className="body">
                <div id='home-page-content'>
                    <HomePageLinks />
                </div>
            </div>

            <div className="footer">
                <Footer page={location.pathname} />
            </div>
        </div>
    ) ;
}

export default HomePage ;