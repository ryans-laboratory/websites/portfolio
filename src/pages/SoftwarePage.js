import React from 'react' ;
import { useLocation } from 'react-router-dom' ;

import CustomScrollbar from '../components/CustomScrollbar' ;
import Header from '../components/Header'
import Footer from '../components/Footer'


function SoftwarePage () {
    let location = useLocation() ;

    return (
        <CustomScrollbar>
            <div id="software-container" className="container">
                <div className="header">
                    <Header page={location.pathname}/>
                </div>

                <div className="body">
                    <div className='software-page-content'>
                        <a href='https://gitlab.com/ryans-laboratory' target='_blank' rel="noreferrer">
                            <div id='gitlab-link'>
                                <span>
                                    <i className="lab la-gitlab"></i>
                                </span>
                                <span>Gitlab</span>
                            </div>
                        </a>

                        <div id="software-examples">
                            <div id="software-examples-text">Some of the things I've enjoyed making...</div>

                            <div id="software-examples-list">
                                <div className='software-example'>
                                    <div className='software-example-title'>
                                       <a href='https://gitlab.com/ryans-laboratory/websites/portfolio' target='_blank' rel='noreferrer'>
                                           <span className="software-example-title-text website">THIS WEBSITE</span>
                                        </a>
                                    </div>
                                    <div className='software-example-description'>
                                        This website is constructed with React and SASS. While it's mostly static and doesn't require the full strength of React,
                                        I wanted to use this as an opportunity to better learn the library.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="footer">
                    <Footer page={location.pathname} />
                </div>
            </div>
        </CustomScrollbar>
    ) ;
}

export default SoftwarePage ;