
import React from 'react' ;
import { useLocation, Link } from 'react-router-dom' ;

import WritingPageLinks from '../components/WritingPageLinks';
import CustomScrollbar from '../components/CustomScrollbar' ;
import Header from '../components/Header'
import Footer from '../components/Footer'


function WritingPage () {
    let location = useLocation() ;

    return (
        <CustomScrollbar>
            <div id="writing-container" className="container">
                <div className="header">
                    <Header page={location.pathname}/>
                </div>

                <div className="body">
                    <div id="writing-page-content">
                        <WritingPageLinks />

                        <div className='writing-page-description'>
                            <p>Occasionally, I write things.</p>
                            <p><Link to='' className='philosophy'>PHILOSOPHY</Link> is where I write about my philosophical musings.</p>
                            <p><a href='https://www.thetechnodruid.com' className='spirituality'>SPIRITUALITY</a> is where I develop my spiritual philosophy.</p>
                            <p><Link to='' className='creative'>CREATIVE</Link> is where I place all of my fiction and poetry creations.</p>
                            <p><Link to='' className='blog'>BLOG</Link> is where I write about things and ideas in my life. A general dumping ground for hobbies and topics that don't belong elsewhere.</p>
                        </div>
                    </div>
                </div>

                <div className="footer">
                    <Footer page={location.pathname} />
                </div>
            </div>
        </CustomScrollbar>
    ) ;
}

export default WritingPage ;