import React from 'react' ;
import { Link , useLocation } from 'react-router-dom' ;

import CustomScrollbar from '../components/CustomScrollbar' ;
import Header from '../components/Header' ;
import Footer from '../components/Footer' ;


function AboutPage () {
    let location = useLocation() ;

    return (
        <CustomScrollbar>
            <div id="about-container" className="container">
                <div className="header">
                    <Header page={location.pathname}/>
                </div>

                <div className="body" id="body">
                    <div id="aboutme-page-content">
                        <a href="https://www.linkedin.com/in/rdekler/" target="_blank" rel="noreferrer">
                            <p id="linkedin-link">
                                <span><i className="lab la-linkedin"></i></span>
                                <span>Resumé</span>
                            </p>
                        </a>
                        <div id="aboutme-paragraphs">
                            <div className='aboutme-paragraph yellow'>
                                <span>\\MY STORY</span>
                                <span>
                                    <p>
                                        I am a Raleigh-based devops engineer. With degrees in physics and English, I began learning and coding hunched over
                                        the keyboard in my college dorm room, making whatever project came into my head. Eventually I wanted to work at scale
                                        and convinced someone to hire me.
                                    </p>
                                    <p>
                                        I've worked as a web developer, software engineer, devops engineer, and site reliability engineer at companies large
                                        and small. The same desire to learn, play, and experiment that got me started down this path still drives me now.
                                    </p>
                                    <p>
                                        As I mature in this field, I've become more committed than ever to facing the problems I see in the tech world head-on.
                                        The most important of which, I believe, is the need to make a switch in mentality about what and why we are creating.
                                        In the end, everything we create is for humans. As such, we must consider all human impacts of our designs.
                                    </p>
                                </span>
                            </div>
                            <div className='aboutme-paragraph red'>
                                <span>\\MY MISSION</span>
                                <span>
                                    <p>
                                        I believe deeply in the ability of software and processes to improve people's lives. While most problems facing
                                        humanity are human and not technological in nature (and it's important that we not delude ourselves into believing
                                        that everything can be solved with technology), we can design technology to aid in finding and implementing the right
                                        solutions.
                                    </p>
                                    <p>
                                        As we face the social, economic, and environmental consequences of the choices made by those who came before us, we
                                        must design a future in which people are better connected to themselves, to others, and to the Earth.
                                    </p>
                                    <p>
                                        Most important in this process is to design the technology involved in these solutions to be for <b>humans</b>. 
                                        And that is where my mission begins: to create a future that's designed around people, not objects or machines. To create
                                        a future that's more human.
                                    </p>
                                </span>
                            </div>
                            <div className='aboutme-paragraph blue'>
                                <span>\\MY PROJECTS</span>
                                <span>
                                    <p>In between work and life I try to create as much as possible.</p>
                                    <p>- You can find out more about what I'm coding on the <Link to='/software' className='software'>SOFTWARE</Link> page.</p>
                                    <p>- You can get inside my head on the <Link to='/writing' className='writing'>WRITING</Link> page.</p>
                                    <p>- You can follow my explorations in architecture on the <Link to='/architecture' className='architecture'>ARCHITECTURE</Link> page.</p>
                                    <p>
                                        And I'm sure, as time goes on, I'll have the desire to showcase more of my interests. I'm not quite 
                                        yet at the point where I want to post my lifting PRs or sewing projects, though they may show up in my writing.
                                    </p>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="footer">
                    <Footer page={location.pathname} />
                </div>
            </div>
        </CustomScrollbar>
    ) ;
}

export default AboutPage ;