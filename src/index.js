import React from 'react';
import { createRoot } from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom' ;
import './styles/index.scss';
import Portfolio from './Portfolio';
import reportWebVitals from './reportWebVitals';


const container = document.getElementById('root') ;
const root = createRoot(container) ;
root.render(
  <BrowserRouter>
    <Portfolio />
  </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals(console.log);
